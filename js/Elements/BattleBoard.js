export default class BattleBoard {
    //要素作成先ID
    #TargetID = "BattleBoard";

    //盤面サイズ
    #BoardSize = [3, 3];

    //盤面作成
    MakeBoard() {
        let target = document.getElementById(this.#TargetID);

        for (let tr_cnt = 0; tr_cnt < this.#BoardSize[0]; tr_cnt++) {
            let tr = document.createElement("tr");

            for (let td_cnt = 0; td_cnt < this.#BoardSize[1]; td_cnt++) {
                let td = document.createElement("td");
                tr.appendChild(td);
            }

            target.appendChild(tr);
        }
    }
}