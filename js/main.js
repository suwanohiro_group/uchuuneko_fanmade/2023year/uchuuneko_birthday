import BattleBoard from "./Elements/BattleBoard.js";

export default class Main {
    Initialize() {
        let Board = new BattleBoard();
        Board.MakeBoard();
    }
}